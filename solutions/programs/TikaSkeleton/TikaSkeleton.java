import org.apache.tika.exception.TikaException;
import org.apache.tika.language.LanguageIdentifier;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TikaSkeleton {

    List<String> keywords;
    PrintWriter logfile;
    int num_keywords, num_files, num_fileswithkeywords;
    Map<String, Integer> keyword_counts;
    Date timestamp;

    /**
     * constructor
     * DO NOT MODIFY
     */
    public TikaSkeleton() {
        keywords = new ArrayList<String>();
        num_keywords = 0;
        num_files = 0;
        num_fileswithkeywords = 0;
        keyword_counts = new HashMap<String, Integer>();
        timestamp = new Date();
        try {
            logfile = new PrintWriter("log.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * main() function
     * instantiate class and execute
     * DO NOT MODIFY
     */
    public static void main(String[] args) {
        TikaSkeleton instance = new TikaSkeleton();
        instance.run();
    }

    /**
     * destructor
     * DO NOT MODIFY
     */
    protected void finalize() throws Throwable {
        try {
            logfile.close();
        } finally {
            super.finalize();
        }
    }

    /**
     * execute the program
     * DO NOT MODIFY
     */
    private void run() {

        // Open input file and read keywords
        try {
            BufferedReader keyword_reader = new BufferedReader(new FileReader("queries.txt"));
            String str;
            while ((str = keyword_reader.readLine()) != null) {
                keywords.add(str);
                num_keywords++;
                keyword_counts.put(str, 0);
            }
            keyword_reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Open all pdf files, process each one
        File pdfdir = new File("./tikadataset");
        File[] pdfs = pdfdir.listFiles(new PDFFilenameFilter());
        for (File pdf : pdfs) {
            num_files++;
            processfile(pdf);
        }

        // Print output file
        try {
            PrintWriter outfile = new PrintWriter("output.txt");
            outfile.print("Keyword(s) used: ");
            if (num_keywords > 0) outfile.print(keywords.get(0));
            for (int i = 1; i < num_keywords; i++) outfile.print(", " + keywords.get(i));
            outfile.println();
            outfile.println("No of files processed: " + num_files);
            outfile.println("No of files containing keyword(s): " + num_fileswithkeywords);
            outfile.println();
            outfile.println("No of occurrences of each keyword:");
            outfile.println("----------------------------------");
            for (int i = 0; i < num_keywords; i++) {
                String keyword = keywords.get(i);
                outfile.println("\t" + keyword + ": " + keyword_counts.get(keyword));
            }
            outfile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process a single file
     * <p>
     * Here, you need to:
     * - use Tika to extract text contents from the file
     * - search the extracted text for the given keywords
     * - update num_fileswithkeywords and keyword_counts as needed
     * - update log file as needed
     *
     * @param f File to be processed
     */
    private void processfile(File f) {
        Metadata metadata = new Metadata();
        PDFParser parser = new PDFParser();
        BodyContentHandler handler = new BodyContentHandler(10000000);
        ParseContext context = new ParseContext();

        try {
            parser.parse(new FileInputStream(f), handler, metadata, context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        }

        String content = handler.toString();

        LanguageIdentifier identifier = new LanguageIdentifier(handler.toString());
        String language = identifier.getLanguage();

        /***** YOUR CODE GOES HERE *****/
        // to update the log file with information on the language, author, type, and last modification date implement
        updatelogMetaData(f.getName(), metadata, language);

        boolean foundAny = false;
        int thisKeywordOccurrences = 0;

        for (String keyword : keywords) {
            Pattern pattern = Pattern.compile(keyword, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(content.replaceAll("\\W+", ""));
            while (matcher.find()) {
                thisKeywordOccurrences++;
                updatelogHit(keyword, f.getName());
            }
            if(thisKeywordOccurrences > 0) {
                foundAny = true;
                keyword_counts.put(keyword, keyword_counts.get(keyword) + thisKeywordOccurrences);
            }
        }

        if(foundAny) {
            num_fileswithkeywords++;
        }
    }

    private void updatelogMetaData(String filename, Metadata metadata, String detectedLanguage) {
        logfile.println(" -- " + " data on file \"" + filename + "\"");
        logfile.println("Detected language: " + detectedLanguage);
        logfile.println("Author: " + metadata.get(Metadata.AUTHOR));
        logfile.println("File type: " + metadata.get(Metadata.CONTENT_TYPE));
        logfile.println("Last modified: " + metadata.get(Metadata.LAST_MODIFIED));
        logfile.println();
        logfile.flush();
    }

    /**
     * Update the log file with search hit
     * Appends a log entry with the system timestamp, keyword found, and filename of PDF file containing the keyword
     * DO NOT MODIFY
     */
    private void updatelogHit(String keyword, String filename) {
        timestamp.setTime(System.currentTimeMillis());
        logfile.println(timestamp + " -- \"" + keyword + "\" found in file \"" + filename + "\"");
        logfile.flush();
    }

    /**
     * Filename filter that accepts only *.pdf
     * DO NOT MODIFY
     */
    static class PDFFilenameFilter implements FilenameFilter {
        private Pattern p = Pattern.compile(".*\\.pdf", Pattern.CASE_INSENSITIVE);

        public boolean accept(File dir, String name) {
            Matcher m = p.matcher(name);
            return m.matches();
        }
    }
}
