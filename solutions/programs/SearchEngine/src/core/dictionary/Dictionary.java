package core.dictionary;

import core.term.PorterTermProcessor;

import java.util.*;

public class Dictionary {
    private final HashMap<String, String> wordToStemMapping = new HashMap<String, String>();

    public void clear() {
        wordToStemMapping.clear();
    }

    public void load(Collection<String> keywords) {
        for (String keyword : keywords) {
            String stem = PorterTermProcessor.getStemmedForm(keyword);
            wordToStemMapping.put(keyword, stem);
        }
    }

    public boolean isKeyword(String stem) {
        return wordToStemMapping.containsValue(stem);
    }

    public Set<Map.Entry<String, String>> entrySet() {
        return wordToStemMapping.entrySet();
    }
}
