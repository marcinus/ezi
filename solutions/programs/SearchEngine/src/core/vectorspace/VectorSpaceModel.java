package core.vectorspace;

import core.dictionary.Dictionary;
import core.document.Document;

import javax.print.Doc;
import java.util.*;
import java.util.stream.Collectors;

public class VectorSpaceModel {
    private final Map<String, Map<String, Double>> termTermCorrelationMatrix = new HashMap<>();
    private final Map<String, Double> invertedDocumentFrequencyLookup = new HashMap<String, Double>();
    private final HashMap<Document, DocumentVector> documentVectors = new HashMap<Document, DocumentVector>();
    private final Collection<Document> documents = new ArrayList<Document>();
    private final Dictionary dictionary = new Dictionary();

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void loadDocuments(Collection<Document> documents) {
        clearDocuments();
        this.documents.addAll(documents);
        update();
    }

    public void loadKeywords(Collection<String> keywords) {
        clearDictionary();
        dictionary.load(keywords);
        update();
    }

    public void clearDocuments() {
        this.documents.clear();
        this.documentVectors.clear();
        this.invertedDocumentFrequencyLookup.clear();
        this.termTermCorrelationMatrix.clear();
    }

    public void clearDictionary() {
        this.dictionary.clear();
        this.documentVectors.clear();
        this.invertedDocumentFrequencyLookup.clear();
        this.termTermCorrelationMatrix.clear();
    }

    public Map<DocumentVector, Double> calculateSimilarityMeasure(DocumentVector query) {
        Map<DocumentVector, Double> similarItems = new HashMap<DocumentVector, Double>();
        for(DocumentVector documentVector : documentVectors.values()) {
            double similarity = VectorSpace.getCosineAngleBetween(documentVector, query);
            similarItems.put(documentVector, similarity);
        }
        return similarItems;
    }

    public double getIDFMeasureFor(String term) {
        if (invertedDocumentFrequencyLookup.containsKey(term)) {
            return invertedDocumentFrequencyLookup.get(term);
        } else {
            return 0;
        }
    }

    public DocumentVector getDocumentVectorFor(Document document) {
        return documentVectors.get(document);
    }

    private void update() {
        if(dictionary != null && !documents.isEmpty()) {
            for (Document document : documents) {
                document.createBagOfWordsRepresentationWithTitleUsing(dictionary);
            }

            calculateIDFMeasureForTerms();
            createVectorRepresentation();

            calculateTermTermCorrelationMatrix();
        }
    }

    private void calculateIDFMeasureForTerms() {
        Map<String, Integer> termDocumentFrequencies = calculateTermDocumentFrequencies();

        int numberOfDocuments = documents.size();

        for (Map.Entry<String, Integer> entry : termDocumentFrequencies.entrySet()) {
            String term = entry.getKey();
            int termDocumentFrequency = entry.getValue();

            double termIDFMeasure = Math.log(numberOfDocuments / termDocumentFrequency);

            invertedDocumentFrequencyLookup.put(term, termIDFMeasure);
        }
    }

    private Map<String, Integer> calculateTermDocumentFrequencies() {
        Map<String, Integer> termDocumentFrequencies = new HashMap<String, Integer>();

        for (Document document : documents) {
            for (String term : document.getTerms()) {
                if (termDocumentFrequencies.containsKey(term)) {
                    termDocumentFrequencies.put(term, termDocumentFrequencies.get(term) + 1);
                } else {
                    termDocumentFrequencies.put(term, 1);
                }
            }
        }

        return termDocumentFrequencies;
    }

    private void createVectorRepresentation() {
        for (Document document : documents) {
            DocumentVector vector = new DocumentVector(document, this);
            documentVectors.put(document, vector);
        }
    }

    private void calculateTermTermCorrelationMatrix() {
        Map<String, Map<Document, Double>> matrixA = new HashMap<>();

        for(Document document : documents) {
            for(String term : document.getTerms()) {

                if(!matrixA.containsKey(term)) {
                    matrixA.put(term, new HashMap<>());
                }

                Map<Document, Double> row = matrixA.get(term);
                row.put(document, new Double(document.getOccurrencesOf(term)));
            }
        }

        for(Map.Entry<String, Map<Document, Double>> row : matrixA.entrySet()) {
            double totalInRow = 0;
            for(Double x : row.getValue().values()) {
                totalInRow += x;
            }

            for(Map.Entry<Document, Double> entry : row.getValue().entrySet()) {
                entry.setValue(entry.getValue() / totalInRow);
                //System.out.println(row.getKey() + "-" + entry.getKey().getTitle() + ": " + entry.getValue());
            }
        }

        for (String termA : invertedDocumentFrequencyLookup.keySet()) {
            Map<String, Double> termTermCorrelationRow = new HashMap<>();
            termTermCorrelationMatrix.put(termA, new HashMap<>());
            for(String termB : invertedDocumentFrequencyLookup.keySet()) {
                double correlationIndex = 0;
                for(Document document : documents) {
                    Map<Document, Double> matrixARow = matrixA.get(termA);
                    Map<Document, Double> matrixAColumn = matrixA.get(termB);
                    if(matrixARow.containsKey(document) && matrixAColumn.containsKey(document)) {
                        correlationIndex += matrixARow.get(document) * matrixAColumn.get(document);
                    }
                }
                termTermCorrelationRow.put(termB, correlationIndex);
            }
            termTermCorrelationMatrix.put(termA, termTermCorrelationRow);
        }
    }

    public List<String> getSimilarTerms(Document query) {
        Map<String, Double> candidates = new HashMap<>();
        for(String term : query.getTerms()) {
            Map<String, Double> currentCandidates = termTermCorrelationMatrix.get(term);
            for (Map.Entry<String, Double> candidate : currentCandidates.entrySet()) {
                if(query.getOccurrencesOf(candidate.getKey()) > 0) continue;
                if(candidates.containsKey(candidate.getKey())) {
                    candidates.put(candidate.getKey(), Math.max(candidate.getValue(), candidates.get(candidate.getKey())));
                } else {
                    candidates.put(candidate.getKey(), candidate.getValue());
                }
            }
        }

        List<Map.Entry<String, Double>> candidatesList = new ArrayList<>(candidates.entrySet());
        Collections.sort(candidatesList, (o1, o2) -> {
            if(o1.getValue() > o2.getValue()) return -1;
            else if(o1.getValue() < o2.getValue()) return 1;
            else return 0;
        });
        return candidatesList.subList(0, Math.min(5, candidatesList.size())).stream().map(x->x.getKey()).collect(Collectors.toList());
    }
}
