package core.vectorspace;

import core.document.Document;
import core.vectorspace.Vector;

import java.util.Map;

public class VectorSpace {

    public static double getNorm(Vector v) {
        double normSquared = 0.0;
        for (double coordinate :
             v.getCoordinates().values()) {
            normSquared += coordinate*coordinate;
        }
        return Math.sqrt(normSquared);
    }

    public static double dotProduct(Vector v1, Vector v2) {
        double product = 0.0;

        for (Map.Entry<String, Double> v1Coordinate :
             v1.getCoordinates().entrySet()) {

            String identifier = v1Coordinate.getKey();
            Double v1CoordinateValue = v1Coordinate.getValue();

            if(v2.getCoordinates().containsKey(identifier)) {
                Double v2CoordinateValue = v2.getCoordinates().get(identifier);

                product += v1CoordinateValue * v2CoordinateValue;
            }

        }

        return product;
    }

    public static double getCosineAngleBetween(Vector v1, Vector v2) {
        Double normv1 = getNorm(v1);
        Double normv2 = getNorm(v2);
        if(normv1 == 0 || normv2 == 0) return 0;
        return dotProduct(v1, v2) / (normv1 * normv2);
    }
}
