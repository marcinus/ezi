package core.vectorspace;

import java.util.Map;

public interface Vector {
    Map<String, Double> getCoordinates();
}
