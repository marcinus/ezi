package core.vectorspace;

import core.document.Document;

import java.util.HashMap;
import java.util.Map;

public class DocumentVector implements Vector {
    private final Map<String, Double> TF_Coefficients;
    private final Map<String, Double> TF_IDF_Coefficients;
    private final Document document;

    public DocumentVector(Document document, VectorSpaceModel vectorSpaceModel) {
        TF_Coefficients = new HashMap<String, Double>();
        TF_IDF_Coefficients = new HashMap<String, Double>();
        this.document = document;

        calculateTFMeasure();
        calculateTFIDFMeasure(vectorSpaceModel);
    }

    public Document getDocument() {
        return document;
    }

    @Override
    public Map<String, Double> getCoordinates() {
        return TF_IDF_Coefficients;
    }

    private void calculateTFMeasure() {
        String mostFrequentWord = document.getMostFrequentWord();
        int maximumOccurrences = document.getOccurrencesOf(mostFrequentWord);

        for (String term : document.getTerms()) {
            double TFMeasure = (double) document.getOccurrencesOf(term) / maximumOccurrences;
            TF_Coefficients.put(term, TFMeasure);
        }
    }

    private void calculateTFIDFMeasure(VectorSpaceModel model) {
        for (Map.Entry<String, Double> entry :
                TF_Coefficients.entrySet()) {
            String term = entry.getKey();
            Double TFMeasure = entry.getValue();
            TF_IDF_Coefficients.put(term, TFMeasure * model.getIDFMeasureFor(term));
        }
    }
}
