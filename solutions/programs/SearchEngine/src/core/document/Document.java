package core.document;

import com.sun.prism.shader.Solid_TextureRGB_AlphaTest_Loader;
import core.dictionary.Dictionary;
import core.term.PorterTermProcessor;

import java.util.*;

public class Document {
    private Dictionary dictionary;
    private final String title;
    private final String content;
    private final List<String> titleStemmed;
    private final List<String> contentStemmed;
    private final Map<String, Integer> bagOfWords = new HashMap<String, Integer>();

    public Document(String title, String content) {
        this.title = title;
        this.content = content;

        titleStemmed = PorterTermProcessor.splitAndStem(title);
        contentStemmed = PorterTermProcessor.splitAndStem(content);
    }

    public void createBagOfWordsRepresentationWithTitleUsing(Dictionary dictionary) {
        createBagOfWordsRepresentationUsing(dictionary);
        extendBagOfWordsBy(titleStemmed);
    }

    public void createBagOfWordsRepresentationUsing(Dictionary dictionary) {
        this.dictionary = dictionary;
        bagOfWords.clear();
        extendBagOfWordsBy(contentStemmed);
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Collection<String> getTerms() {
        return bagOfWords.keySet();
    }

    public int getOccurrencesOf(String term) {
        if (bagOfWords.containsKey(term)) {
            return bagOfWords.get(term);
        } else {
            return 0;
        }
    }

    public String getMostFrequentWord() {
        String mostFrequentWord = null;
        int maximumOccurrences = 0;

        for (Map.Entry<String, Integer> entry :
                bagOfWords.entrySet()) {

            if (entry.getValue() > maximumOccurrences) {
                maximumOccurrences = entry.getValue();
                mostFrequentWord = entry.getKey();
            }
        }

        return mostFrequentWord;
    }

    private void extendBagOfWordsBy(List<String> textStemmed) {
        for (String stem : textStemmed) {
            if (bagOfWords.containsKey(stem)) {
                int occurrences = bagOfWords.get(stem);
                bagOfWords.put(stem, occurrences + 1);

            } else if (dictionary.isKeyword(stem)) {
                bagOfWords.put(stem, 1);
            }
        }
    }

    public String getTitleStemmed() {
        return String.join(" ", titleStemmed);
    }

    public String getContentStemmed() {
        return String.join(" ", contentStemmed);
    }
}
