package core.term;

import external.Stemmer;

import java.util.ArrayList;
import java.util.List;

public class PorterTermProcessor {
    private static final Stemmer porterStemmer = new Stemmer();

    public static String getStemmedForm(String word) {
        porterStemmer.add(word.toCharArray(), word.length());
        porterStemmer.stem();
        return porterStemmer.toString();
    }

    public static List<String> splitAndStem(String text) {
        List<String> stemmedText = new ArrayList<String>();

        for (String word :
                text.split("[^a-zA-Z0-9']+")) {

            String term = getStemmedForm(word.toLowerCase());

            if(!term.isEmpty()) {
                stemmedText.add(term);
            }
        }

        return stemmedText;
    }
}
