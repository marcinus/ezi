package fx.controller;

import core.document.Document;
import io.DocumentReader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import run.Main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class DocumentsController implements Initializable {

    private ObservableList<Document> documentObservableList = FXCollections.observableArrayList();

    @FXML
    private Button loadDocumentsButton;
    @FXML
    private TableView<Document> documentsTable;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        documentsTable.setItems(documentObservableList);
        documentsTable.setRowFactory(tv -> {
            TableRow<Document> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Document document = row.getItem();
                    Stage detailsStage = new Stage();
                    Parent documentDetailsRoot = null;
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fx/view/DocumentDetails.fxml"));
                        documentDetailsRoot = loader.load();
                        DocumentDetailsController documentDetailsController = loader.getController();
                        documentDetailsController.setDocumentVector(Main.vectorSpaceModel.getDocumentVectorFor(document));
                        Main.openedWindows.put(detailsStage, documentDetailsController);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    detailsStage.setTitle("Document Details");
                    detailsStage.setResizable(false);
                    detailsStage.setScene(new Scene(documentDetailsRoot));
                    detailsStage.show();
                }
            });
            return row;
        });
    }

    @FXML
    protected void loadDocuments(ActionEvent event) {
        FileChooser.ExtensionFilter txtFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open DOCUMENTS file");
        fileChooser.getExtensionFilters().add(txtFilter);
        File documentsFile = fileChooser.showOpenDialog(loadDocumentsButton.getScene().getWindow());

        if (documentsFile != null) {
            for(Stage stage : Main.openedWindows.keySet()) {
                stage.close();
            }
            Collection<Document> documents = DocumentReader.read(documentsFile);
            Main.vectorSpaceModel.loadDocuments(documents);
            documentObservableList.clear();
            documentObservableList.addAll(documents);

            Main.keywordsController.recalculateKeywordsView();
        }
    }

    @FXML
    protected void viewDocumentDetails(ActionEvent event) {

    }
}
