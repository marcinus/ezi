package fx.controller;

import core.document.Document;
import core.vectorspace.DocumentVector;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import run.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static run.Main.vectorSpaceModel;

public class SearchBoxController implements Initializable {

    private ObservableList<Map.Entry<DocumentVector, Double>> matchingDocumentsRanking = FXCollections.observableArrayList();
    private ObservableList<DocumentDetailsController.BagOfWordsEntryView> queryKeywords = FXCollections.observableArrayList();

    @FXML
    private TextField searchPhrase;

    @FXML
    private Label stemmedSearchPhrase;

    @FXML
    private HBox expansionsBox;

    @FXML
    private TableColumn<Map.Entry<DocumentVector, Double>, String> documentTitleColumn;

    @FXML
    private TableColumn<Map.Entry<DocumentVector, Double>, Double> documentSimilarityColumn;

    @FXML
    private TableView<Map.Entry<DocumentVector, Double>> documentsRatingTable;

    @FXML
    private TableColumn queryTFIDFColumn;

    @FXML
    private TableView<DocumentDetailsController.BagOfWordsEntryView> keywordSummary;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        keywordSummary.setItems(queryKeywords);
        queryTFIDFColumn.setSortType(TableColumn.SortType.DESCENDING);
        keywordSummary.getSortOrder().add(queryTFIDFColumn);
        searchPhrase.textProperty().addListener(((observable, oldValue, newValue) -> search(newValue)));
        documentsRatingTable.setItems(matchingDocumentsRanking);
        documentTitleColumn.setCellValueFactory((TableColumn.CellDataFeatures<Map.Entry<DocumentVector, Double>, String> p) -> new SimpleStringProperty(p.getValue().getKey().getDocument().getTitle()));
        documentSimilarityColumn.setCellValueFactory((TableColumn.CellDataFeatures<Map.Entry<DocumentVector, Double>, Double> p) -> new SimpleObjectProperty<Double>(p.getValue().getValue()));
        documentSimilarityColumn.setSortType(TableColumn.SortType.DESCENDING);
        documentsRatingTable.getSortOrder().add(documentSimilarityColumn);

        documentsRatingTable.setRowFactory(tv -> {
            TableRow<Map.Entry<DocumentVector, Double>> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Document document = row.getItem().getKey().getDocument();
                    Stage detailsStage = new Stage();
                    Parent documentDetailsRoot = null;
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fx/view/DocumentDetails.fxml"));
                        documentDetailsRoot = loader.load();
                        DocumentDetailsController documentDetailsController = loader.getController();
                        documentDetailsController.setDocumentVector(Main.vectorSpaceModel.getDocumentVectorFor(document));
                        Main.openedWindows.put(detailsStage, documentDetailsController);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    detailsStage.setTitle("Document Details");
                    detailsStage.setResizable(false);
                    detailsStage.setScene(new Scene(documentDetailsRoot));
                    detailsStage.show();
                }
            });
            return row;
        });
    }

    public void setNewQuery(String query) {
        searchPhrase.setText(searchPhrase.getText() + " " + query);
    }

    @FXML
    public void search(String query) {
        Document queryDocument = new Document("", query);

        queryDocument.createBagOfWordsRepresentationUsing(vectorSpaceModel.getDictionary());

        List<String> expansions = Main.vectorSpaceModel.getSimilarTerms(queryDocument);

        expansionsBox.getChildren().clear();

        List<Hyperlink> hyperlinks = new ArrayList<>();

        for(String expansion : expansions) {
            Hyperlink hyperlink = new Hyperlink(expansion);
            hyperlink.setOnAction(event -> setNewQuery(expansion));
            hyperlinks.add(hyperlink);
        }

        expansionsBox.getChildren().addAll(hyperlinks);

        DocumentVector queryVector = new DocumentVector(queryDocument, Main.vectorSpaceModel);

        stemmedSearchPhrase.setText(queryDocument.getContentStemmed());

        Map<DocumentVector, Double> documentsReturned = Main.vectorSpaceModel.calculateSimilarityMeasure(queryVector);

        matchingDocumentsRanking.clear();
        documentsReturned = documentsReturned.entrySet().stream().filter(map -> map.getValue() > 0).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
        matchingDocumentsRanking.addAll(documentsReturned.entrySet());

        documentsRatingTable.sort();
        refreshBagOfWordsRepresentation(queryVector);
    }

    public void refreshBagOfWordsRepresentation(DocumentVector queryVector) {
        queryKeywords.clear();
        for(Map.Entry<String, Double> entry : queryVector.getCoordinates().entrySet()) {
            DocumentDetailsController.BagOfWordsEntryView view = new DocumentDetailsController.BagOfWordsEntryView(entry, queryVector);
            queryKeywords.add(view);
        }
        keywordSummary.sort();
    }
}
