package fx.controller;

import core.dictionary.Dictionary;
import io.KeywordsReader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import run.Main;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.ResourceBundle;

public class KeywordsController implements Initializable {

    private ObservableList<KeywordEntryView> keywordEntriesViews = FXCollections.observableArrayList();
    @FXML
    private Button loadKeywordsButton;
    @FXML
    private TableView<KeywordEntryView> keywordsTable;

    @FXML
    private TableColumn IDFColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        keywordsTable.setItems(keywordEntriesViews);
        IDFColumn.setSortType(TableColumn.SortType.DESCENDING);
        keywordsTable.getSortOrder().add(IDFColumn);
    }

    @FXML
    protected void loadKeywords(ActionEvent event) {
        FileChooser.ExtensionFilter txtFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open KEYWORDS file");
        fileChooser.getExtensionFilters().add(txtFilter);
        File keywordsFile = fileChooser.showOpenDialog(loadKeywordsButton.getScene().getWindow());

        if (keywordsFile != null) {
            Collection<String> keywords = KeywordsReader.read(keywordsFile);
            Main.vectorSpaceModel.loadKeywords(keywords);
            recalculateKeywordsView();
        }
    }

    public void recalculateKeywordsView() {
        keywordEntriesViews.clear();
        Dictionary dictionary = Main.vectorSpaceModel.getDictionary();
        for (Map.Entry<String, String> keywordEntry : dictionary.entrySet()) {
            KeywordEntryView viewEntry = new KeywordEntryView();
            viewEntry.keyword = keywordEntry.getKey();
            viewEntry.stemmedForm = keywordEntry.getValue();
            viewEntry.IDFCoefficient = Main.vectorSpaceModel.getIDFMeasureFor(keywordEntry.getValue());
            keywordEntriesViews.add(viewEntry);
        }
        keywordsTable.sort();
    }

    public class KeywordEntryView {
        String keyword;
        String stemmedForm;
        Double IDFCoefficient;

        public String getKeyword() {
            return keyword;
        }

        public String getStemmedForm() {
            return stemmedForm;
        }

        public Double getIDFCoefficient() {
            return IDFCoefficient;
        }
    }
}
