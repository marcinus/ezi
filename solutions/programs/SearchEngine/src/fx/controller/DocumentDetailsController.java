package fx.controller;

import core.document.Document;
import core.vectorspace.DocumentVector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

public class DocumentDetailsController implements Initializable {
    private DocumentVector documentVector;
    private ObservableList<BagOfWordsEntryView> bagOfWordsEntryViews = FXCollections.observableArrayList();

    public static class BagOfWordsEntryView {

        String keyword;
        int numberOfOccurrences;
        double TFIDFMeasure;

        public BagOfWordsEntryView(Map.Entry<String, Double> entry, DocumentVector documentVector) {
            keyword = entry.getKey();
            TFIDFMeasure = entry.getValue();
            numberOfOccurrences = documentVector.getDocument().getOccurrencesOf(entry.getKey());
        }

        public String getKeyword() {
            return keyword;
        }

        public int getNumberOfOccurrences() {
            return numberOfOccurrences;
        }

        public double getTFIDFMeasure() {
            return TFIDFMeasure;
        }

    }
    @FXML
    private Label titleLabel;

    @FXML
    private Label titleStemmedLabel;

    @FXML
    private Label contentLabel;

    @FXML
    private Label contentStemmedLabel;

    @FXML
    private TableColumn TFIDFColumn;

    @FXML
    private TableView<BagOfWordsEntryView> bagOfWordsTable;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bagOfWordsTable.setItems(bagOfWordsEntryViews);
        TFIDFColumn.setSortType(TableColumn.SortType.DESCENDING);
        bagOfWordsTable.getSortOrder().add(TFIDFColumn);
    }

    public void setDocumentVector(DocumentVector documentVector) {
        this.documentVector = documentVector;

        Document document = documentVector.getDocument();

        titleLabel.setText(document.getTitle());
        titleStemmedLabel.setText(document.getTitleStemmed());
        contentLabel.setText(document.getContent());
        contentStemmedLabel.setText(document.getContentStemmed());
        refreshBagOfWordsRepresentation();
    }

    public void refreshBagOfWordsRepresentation() {
        bagOfWordsEntryViews.clear();
        for(Map.Entry<String, Double> entry : documentVector.getCoordinates().entrySet()) {
            BagOfWordsEntryView view = new BagOfWordsEntryView(entry, documentVector);
            bagOfWordsEntryViews.add(view);
        }
        bagOfWordsTable.sort();
    }
}
