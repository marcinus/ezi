package run;

import core.vectorspace.VectorSpaceModel;
import fx.controller.DocumentDetailsController;
import fx.controller.DocumentsController;
import fx.controller.KeywordsController;
import fx.controller.SearchBoxController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.HashMap;
import java.util.Map;

public class Main extends Application {
    public static final VectorSpaceModel vectorSpaceModel = new VectorSpaceModel();
    public static final Map<Stage, DocumentDetailsController> openedWindows = new HashMap<>();
    public static KeywordsController keywordsController;
    public static DocumentsController documentsController;
    public static SearchBoxController searchBoxController;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage keywordsStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../fx/view/Keywords.fxml"));
        Parent keywordsRoot = loader.load();
        keywordsStage.setTitle("Keywords");
        keywordsStage.setResizable(false);
        keywordsStage.setScene(new Scene(keywordsRoot));

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        keywordsStage.show();
        keywordsStage.setX((screenBounds.getWidth() - keywordsStage.getWidth()) / 5);
        keywordsStage.setY((screenBounds.getHeight() - keywordsStage.getHeight()) / 3);

        keywordsStage.setOnCloseRequest(event -> Platform.exit());

        keywordsController = loader.getController();

        Stage documentsStage = new Stage();
        FXMLLoader documentsLoader = new FXMLLoader(getClass().getResource("../fx/view/Documents.fxml"));
        Parent documentsRoot = documentsLoader.load();
        documentsStage.setTitle("Documents");
        documentsStage.setResizable(false);
        documentsStage.setScene(new Scene(documentsRoot));
        documentsStage.show();
        documentsStage.setX((screenBounds.getWidth() - documentsStage.getWidth()) * 4 / 5);
        documentsStage.setY((screenBounds.getHeight() - documentsStage.getHeight()) / 3);

        documentsController = documentsLoader.getController();

        documentsStage.setOnCloseRequest(event -> Platform.exit());

        Stage searchBoxStage = new Stage();
        FXMLLoader searchBoxLoader = new FXMLLoader(getClass().getResource("../fx/view/SearchBox.fxml"));
        Parent searchBoxRoot = searchBoxLoader.load();
        searchBoxStage.setTitle("Simple Search Engine");
        searchBoxStage.setResizable(false);
        searchBoxStage.setScene(new Scene(searchBoxRoot));
        searchBoxStage.show();
        searchBoxStage.setX((screenBounds.getWidth() - searchBoxStage.getWidth()) / 2);
        searchBoxStage.setY((screenBounds.getHeight() - searchBoxStage.getHeight()) / 2);

        searchBoxController = searchBoxLoader.getController();

        searchBoxStage.setOnCloseRequest(event -> Platform.exit());

    }
}
