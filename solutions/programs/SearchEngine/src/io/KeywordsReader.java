package io;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;

public class KeywordsReader {
    public static Collection<String> read(String keywordsFilePath) {
        File keywordsFile = new File(keywordsFilePath);
        return read(keywordsFile);
    }

    public static Collection<String> read(File keywordsFile) {
        Collection<String> keywords = new HashSet<String>();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(keywordsFile));

            String keyword;

            while ((keyword = reader.readLine()) != null) {
                keywords.add(keyword);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                // Nothing to do
            }
        }

        return keywords;
    }
}
