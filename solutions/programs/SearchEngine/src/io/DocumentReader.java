package io;

import core.document.Document;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class DocumentReader {

    private enum DocumentFileReadingState {
        BEFORE_TITLE, BEFORE_CONTENT, AFTER_CONTENT;
    }

    public static Collection<Document> read(String documentsFilePath) {
        File documentsFile = new File(documentsFilePath);
        return read(documentsFile);
    }

    public static Collection<Document> read(File documentsFile) {
        Collection<Document> documents = new HashSet<Document>();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(documentsFile));

            Collection<String> lines = new ArrayList<String>();
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }

            DocumentFileReadingState state = DocumentFileReadingState.BEFORE_TITLE;
            String title = "", content = "";

            for (String documentLine : lines) {
                if(!documentLine.isEmpty()) {
                    if(state == DocumentFileReadingState.BEFORE_TITLE) {
                        title = documentLine;
                        content = "";
                        state = DocumentFileReadingState.BEFORE_CONTENT;
                    } else {
                        content += documentLine + "\n";
                        state = DocumentFileReadingState.AFTER_CONTENT;
                    }
                } else {
                    if(state == DocumentFileReadingState.AFTER_CONTENT) {
                        Document document = new Document(title, content);
                        documents.add(document);
                        state = DocumentFileReadingState.BEFORE_TITLE;
                    }
                    else {
                        throw new RuntimeException("Malformed file");
                    }
                }
            }

            if(state == DocumentFileReadingState.AFTER_CONTENT) {
                Document document = new Document(title, content);
                documents.add(document);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                // Nothing to do
            }
        }

        return documents;
    }
}
