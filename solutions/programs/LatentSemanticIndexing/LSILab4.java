import Jama.Matrix;
import Jama.SingularValueDecomposition;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;

public class LSILab4 {
    Matrix M;
    Matrix Q;

    public static void main(String[] args) {
        LSILab4 lsi = new LSILab4();
        lsi.go();
    }

    private void go() {
        // init the matrix and the query
        M = readMatrix("data.txt");
        Q = readMatrix("query.txt");

        // print
        System.out.print("Matrix:");
        M.print(3, 2);

        // print the dimensions of the matrix
        System.out.println("M: " + dim(M));
        System.out.println();
        // print the query
        System.out.println("Query:");
        Q.print(3, 2);
        System.out.println("Q: " + dim(Q));

        // do svd
        svd();
    }

    private void svd() {

        // get K, S, and D

        SingularValueDecomposition svd = new SingularValueDecomposition(M);

        Matrix K = svd.getU();
        Matrix S = svd.getS();
        Matrix D = svd.getV();

        System.out.println();

        System.out.println("K: " + dim(K));
        System.out.println("S: " + dim(S));
        System.out.println("D: " + dim(D));

        System.out.println();

        //K.print(3, 2);
        //S.print(3, 2);
        //D_t.print(3, 2);

        // set number of largest singular values to be considered
        int s = 4;

        System.out.println("s = " + s);

        System.out.println();

        // cut off appropriate columns and rows from K, S, and D

        Matrix K_s = K.getMatrix(0, K.getRowDimension() - 1, 0, s - 1);
        Matrix S_s = S.getMatrix(0, s - 1, 0, s - 1);
        Matrix D_t_s = D.transpose().getMatrix(0, s - 1, 0, D.getColumnDimension() - 1);

        System.out.println("KS: " + dim(K_s));
        K_s.print(3, 2);

        System.out.println("SS: " + dim(S_s));
        S_s.print(3, 2);

        System.out.println("DST: " + dim(D_t_s));
        D_t_s.print(3, 2);

        // transform the query vector

        Matrix Q_s = Q.transpose().times(K_s).times(S_s.inverse());

        System.out.println("QS: " + dim(Q_s));
        Q_s.print(3, 2);

        System.out.println("Similarities: ");

        // compute similarity of the query and each of the documents, using cosine measure

        for (int i = 0; i < D_t_s.getColumnDimension(); i++) {
            Matrix D_t_s_ith_column = D_t_s.getMatrix(0, s - 1, i, i);

            double sim = Q_s.times(D_t_s_ith_column).get(0, 0);
            sim /= (D_t_s_ith_column.norm2() * Q_s.norm2());

            System.out.println("Doc " + (i+1) + ": " + sim);
        }
    }


    // returns the dimensions of a matrix
    private String dim(Matrix M) {
        return M.getRowDimension() + "x" + M.getColumnDimension();
    }

    // reads a matrix from a file
    private Matrix readMatrix(String filename) {
        Vector<Vector<Double>> m = new Vector<Vector<Double>>();
        int colums = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            while (br.ready()) {
                Vector<Double> row = new Vector<Double>();
                m.add(row);
                String line = br.readLine().trim();
                StringTokenizer st = new StringTokenizer(line, ", ");
                colums = 0;
                while (st.hasMoreTokens()) {
                    row.add(Double.parseDouble(st.nextToken()));
                    colums++;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int rows = m.size();
        Matrix M = new Matrix(rows, colums);
        int rowI = 0;
        for (Vector<Double> vector : m) {
            int colI = 0;
            for (Double d : vector) {
                M.set(rowI, colI, d);
                colI++;
            }
            rowI++;
        }
        return M;
    }
}
