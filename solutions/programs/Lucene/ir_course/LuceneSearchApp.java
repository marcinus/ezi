/*
 * Skeleton class for the Lucene search program implementation
 */
package ir_course;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LuceneSearchApp {
    public static final String indexPath = "index";
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public LuceneSearchApp() {

    }

    public static void main(String[] args) {
        if (args.length > 0) {
            LuceneSearchApp engine = new LuceneSearchApp();

            RssFeedParser parser = new RssFeedParser();
            parser.parse(args[0]);
            List<RssFeedDocument> docs = parser.getDocuments();

            engine.index(docs);

            List<String> inTitle;
            List<String> notInTitle;
            List<String> inDescription;
            List<String> notInDescription;
            List<String> results;

            // 1) search documents with words "kim" and "korea" in the title
            inTitle = new LinkedList<String>();
            inTitle.add("kim");
            inTitle.add("korea");
            results = engine.search(inTitle, null, null, null, null, null);
            engine.printResults(results);

            // 2) search documents with word "kim" in the title and no word "korea" in the description
            inTitle = new LinkedList<String>();
            notInDescription = new LinkedList<String>();
            inTitle.add("kim");
            notInDescription.add("korea");
            results = engine.search(inTitle, null, null, notInDescription, null, null);
            engine.printResults(results);

            // 3) search documents with word "us" in the title, no word "dawn" in the title and word "" and "" in the description
            inTitle = new LinkedList<String>();
            inTitle.add("us");
            notInTitle = new LinkedList<String>();
            notInTitle.add("dawn");
            inDescription = new LinkedList<String>();
            inDescription.add("american");
            inDescription.add("confession");
            results = engine.search(inTitle, notInTitle, inDescription, null, null, null);
            engine.printResults(results);

            // 4) search documents whose publication date is 2011-12-18
            results = engine.search(null, null, null, null, "2011-12-18", "2011-12-18");
            engine.printResults(results);

            // 5) search documents with word "video" in the title whose publication date is 2000-01-01 or later
            inTitle = new LinkedList<String>();
            inTitle.add("video");
            results = engine.search(inTitle, null, null, null, "2000-01-01", null);
            engine.printResults(results);

            // 6) search documents with no word "canada" or "iraq" or "israel" in the description whose publication date is 2011-12-18 or earlier
            notInDescription = new LinkedList<String>();
            notInDescription.add("canada");
            notInDescription.add("iraq");
            notInDescription.add("israel");
            results = engine.search(null, null, null, notInDescription, null, "2011-12-18");
            engine.printResults(results);
        } else
            System.out.println("ERROR: the path of a RSS Feed file has to be passed as a command line argument.");
    }

    public static IndexReader getIndexReader() throws IOException {
        Path path = Paths.get(indexPath);
        Directory directory = FSDirectory.open(path);
        IndexReader indexReader = DirectoryReader.open(directory);
        return indexReader;
    }

    public static IndexSearcher getIndexSearcher(IndexReader indexReader) {
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        return indexSearcher;
    }

    public void index(List<RssFeedDocument> docs) {
        Path iPath = Paths.get(indexPath);
        Directory directory;
        try {
            directory = FSDirectory.open(iPath);
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
            indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);

            for (RssFeedDocument doc : docs) {
                Document document = new Document();
                Field titleField = new TextField("title", doc.getTitle(), Field.Store.YES);
                document.add(titleField);
                Field descriptionField = new TextField("description", doc.getDescription(), Field.Store.YES);
                document.add(descriptionField);
                Field dateField = new StringField("date", dateFormat.format(doc.getPubDate()), Field.Store.YES);
                document.add(dateField);

                indexWriter.addDocument(document);
            }

            indexWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> search(List<String> inTitle, List<String> notInTitle, List<String> inDescription, List<String> notInDescription, String startDate, String endDate) {

        printQuery(inTitle, notInTitle, inDescription, notInDescription, startDate, endDate);

        List<String> results = new LinkedList<String>();

        Analyzer analyzer = new StandardAnalyzer();
        QueryParser queryParser = new QueryParser("title", analyzer);

        String queryString = "";

        if (inTitle != null && !inTitle.isEmpty()) {
            for (String inTitleQuery : inTitle) {
                queryString += "+title:\"" + inTitleQuery + "\"";
            }
        }

        if (notInTitle != null && !notInTitle.isEmpty()) {
            for (String notInTitleQuery : notInTitle) {
                queryString += "-title:\"" + notInTitleQuery + "\"";
            }
        }

        if (inDescription != null && !inDescription.isEmpty()) {
            for (String inDescriptionQuery : inDescription) {
                queryString += "+description:\"" + inDescriptionQuery + "\"";
            }
        }

        if (notInDescription != null && !notInDescription.isEmpty()) {
            for (String notInDescriptionQuery : notInDescription) {
                queryString += "-description:\"" + notInDescriptionQuery + "\"";
            }
        }

        if (startDate != null || endDate != null) {
            if (startDate == null) startDate = "*";
            if (endDate == null) endDate = "*";
            queryString += "+date:[" + startDate + " TO " + endDate + "]";
        }

        try {
            IndexReader indexReader = getIndexReader();
            IndexSearcher indexSearcher = getIndexSearcher(indexReader);
            Query query = queryParser.parse(queryString);
            TopDocs topDocs = indexSearcher.search(query, indexReader.numDocs());

            ScoreDoc[] hits = topDocs.scoreDocs;

            for (ScoreDoc hit : hits) {
                results.add(indexSearcher.doc(hit.doc).getField("title").stringValue());
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // implement the Lucene search here

        return results;
    }

    public void printQuery(List<String> inTitle, List<String> notInTitle, List<String> inDescription, List<String> notInDescription, String startDate, String endDate) {
        System.out.print("Search (");
        if (inTitle != null) {
            System.out.print("in title: " + inTitle);
            if (notInTitle != null || inDescription != null || notInDescription != null || startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (notInTitle != null) {
            System.out.print("not in title: " + notInTitle);
            if (inDescription != null || notInDescription != null || startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (inDescription != null) {
            System.out.print("in description: " + inDescription);
            if (notInDescription != null || startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (notInDescription != null) {
            System.out.print("not in description: " + notInDescription);
            if (startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (startDate != null) {
            System.out.print("startDate: " + startDate);
            if (endDate != null)
                System.out.print("; ");
        }
        if (endDate != null)
            System.out.print("endDate: " + endDate);
        System.out.println("):");
    }

    public void printResults(List<String> results) {
        if (results.size() > 0) {
            Collections.sort(results);
            for (int i = 0; i < results.size(); i++)
                System.out.println(" " + (i + 1) + ". " + results.get(i));
        } else
            System.out.println(" no results");
    }
}
