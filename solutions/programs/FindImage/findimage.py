#!/usr/bin/env python

from os import listdir
from sys import argv
from enum import Enum
from math import sqrt

import numpy as np
import operator

# Constant definitions

featureFilesDir = "features"
imageFilesDir = "Images"

class Aggregation(Enum):
    AVERAGE = "ave"
    MAXIMUM = "max"
    MINIMUM = "min"

class FeatureType(Enum):
    RGB = "ColorRGBCov"
    HCL = "ColorHCLCov"
    LAB = "ColorLabCov"
    HIST64 = "ColorHist64"
    HIST256 = "ColorHist256"
    TEXLUM = "TextureLumGabor"

# Global variables

features = dict()
sigmas = dict()
imageFileNames = []

# Utility functions

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def listFiles(directory, extension):
    return [f for f in listdir(directory) if f.endswith('.' + extension)]

def getImageFileNameIndex(imageFileName):
    global imageFileNames
    if imageFileName not in imageFileNames:
        imageFileNames.append(imageFileName)
    return imageFileNames.index(imageFileName)

def createFeatureMatrixFromLines(featureLines):
    coefficients = dict()

    for line in featureLines:
        lineEntries = line.split(' ')
        fileName = lineEntries[0]
        rowIndex = getImageFileNameIndex(fileName)
        coefficients[rowIndex] = [float(x) for x in lineEntries[1:-1]]

    return coefficients

def calculateSigmasForFeatureMatrix(coefficients):
    return np.matrix(coefficients.values()).std(0).flatten()

def loadImageNames():
    global imageFileNames
    imageFileNames = listFiles(imageFilesDir, "jpeg")

def loadFeatures():
    global features, sigmas
    for featureType in FeatureType:
        featureFileName = featureFilesDir + "/" + featureType.value + ".dat"
        featureFile = open(featureFileName)
        featureLines = featureFile.readlines()
        features[featureType] = createFeatureMatrixFromLines(featureLines)
        sigmas[featureType] = calculateSigmasForFeatureMatrix(features[featureType]).tolist()[0]

def getL1Distance(imageA, imageB, featureType):
    global sigmas
    distance = 0.0
    for x,y,s in zip(imageA, imageB, sigmas[featureType]):
        distance += abs(x-y)/s
    return distance

def getSimilarImages(imageName, featureType):
    global features
    imageA = features[featureType][getImageFileNameIndex(imageName)]
    ranking = dict()
    maxDistance = 0
    for comparedImageNo,imageB in features[featureType].items():
        distance = getL1Distance(imageA, imageB, featureType)
        if distance > maxDistance:
            maxDistance = distance        
        ranking[comparedImageNo] = distance

    for comparedImageNo,distance in ranking.items():
        ranking[comparedImageNo] = 1 - distance / maxDistance

    return sorted(ranking.items(), key=operator.itemgetter(1), reverse=True)

def printTopElementsInRanking(ranking, n):
    for i in range(min(n, len(ranking))):
        item = ranking[i]
        print(imageFileNames[item[0]] + ": " + str(item[1]))

def aggregate(rankings, aggregation):
    rankingDicts = []
    outRanking = dict()
    for ranking in rankings:
        rankingDicts.append(dict(ranking))
        for item in ranking:
            outRanking[item[0]] = 0.0

    if aggregation == Aggregation.AVERAGE:
        n = len(rankings)
        for image in outRanking.keys():
            for ranking in rankingDicts:
                if image in ranking.keys():
                    outRanking[image] += ranking[image]
            outRanking[image] /= n
    
    elif aggregation == Aggregation.MAXIMUM:
        for image in outRanking.keys():
            for ranking in rankingDicts:
                if image in ranking.keys():
                    outRanking[image] = max(outRanking[image], ranking[image])

    elif aggregation == Aggregation.MINIMUM:
        for image in outRanking.keys():
            outRanking[image] = 1.0
            for ranking in rankingDicts:
                if image in ranking.keys():
                    outRanking[image] = min(outRanking[image], ranking[image])
                else:
                    outRanking[image] = 0.0
                    break

    return sorted(outRanking.items(), key=operator.itemgetter(1), reverse=True)

def multicriteriaRanking(pictureAggregation, searchedImageNames, featureAggregation, features):
    imageLevelRankings = []
    for image in searchedImageNames:
        print("Image: " + image)
        featureLevelRankings = []
        for featureType in features:
            print("FeatureType: " + featureType.value)
            ranking = getSimilarImages(image, featureType)
            printTopElementsInRanking(ranking, 12)
            featureLevelRankings.append(ranking)
        aggregatedRanking = aggregate(featureLevelRankings, featureAggregation)
        print("Aggregated for features")
        printTopElementsInRanking(aggregatedRanking, 12)
        imageLevelRankings.append(aggregatedRanking)
    return aggregate(imageLevelRankings, pictureAggregation)

# Main function

def main():
    global imageFileNames

    aggregationNames = [item.value for item in Aggregation]
    featureTypeNames = [item.value for item in FeatureType]

    loadImageNames()
    loadFeatures()

    if len(argv) == 3 and argv[1] in imageFileNames and argv[2] in featureTypeNames:
        ranking = getSimilarImages(argv[1], FeatureType(argv[2]))
        printTopElementsInRanking(ranking, 12)

    elif argv[1] in aggregationNames and RepresentsInt(argv[2]):
        pictureAggregation = Aggregation(argv[1])
        n = int(argv[2])
        if len(argv) > (n+3):
            searchedImageNames = argv[3:(n+3)]
            if argv[n+3] in aggregationNames and RepresentsInt(argv[n+4]):
                featureAggregation = Aggregation(argv[n+3])
                m = int(argv[n+4])
                if len(argv) == n+m+5:
                    features = []
                    for i in range(n+5,n+m+5):
                        features.append(FeatureType(argv[i]))

                    finalRanking = multicriteriaRanking(pictureAggregation, searchedImageNames, featureAggregation, features)
                    print("Final ranking:")
                    printTopElementsInRanking(finalRanking, 12)            

if __name__ == "__main__":
    main()
