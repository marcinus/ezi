import ir.webutils.Graph;
import ir.webutils.Node;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class PageRankFile {
    public static final String PageRankFileName = "pageRanks.txt";

    public static void savePageRanksToFile(Map<Node, Double> pageRanks, File directory) throws FileNotFoundException {
        try (PrintWriter printWriter = new PrintWriter(new File(directory, PageRankFileName))) {
            for (Map.Entry<Node, Double> entry : pageRanks.entrySet()) {
                printWriter.write(entry.getKey().toString() + " " + entry.getValue() + "\n");
            }
        }
    }

    public static String printPageRanksToString(Map<Node, Double> pageRanks) {
        String result = "";
        for (Map.Entry<Node, Double> entry : pageRanks.entrySet()) {
            result += "PR(" + entry.getKey().toString() + ") " + entry.getValue() + "\n";
        }
        return result;
    }

    public static Map<Node, Double> calculatePageRank(Graph graph, double alpha, int iterations) {
        Map<Node, Double> pageRanks = new HashMap<>();
        Node[] nodes = graph.nodeArray();

        for (Node node : nodes) {
            pageRanks.put(node, 1.0);
        }

        normalizePageRanks(pageRanks);

        for (int i = 0; i < iterations; i++) {
            Map<Node, Double> newPageRanks = new HashMap<>();
            for (Node node : nodes) {
                double newPR = alpha;
                for (Object inGoing : node.getEdgesIn()) {
                    Node inGoingNode = (Node) inGoing;
                    newPR += (1 - alpha) * pageRanks.get(inGoingNode) / inGoingNode.getEdgesOut().size();
                }
                newPageRanks.put(node, newPR);
            }
            normalizePageRanks(newPageRanks);
            pageRanks = newPageRanks;
        }

        return pageRanks;
    }

    private static void normalizePageRanks(Map<Node, Double> pageRanks) {
        double total = 0;
        for (Double pr : pageRanks.values()) {
            total += pr;
        }

        for (Map.Entry<Node, Double> entry : pageRanks.entrySet()) {
            pageRanks.put(entry.getKey(), entry.getValue() / total);
        }
    }
}
