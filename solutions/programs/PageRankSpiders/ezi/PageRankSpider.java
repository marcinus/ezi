import ir.utilities.MoreMath;
import ir.utilities.MoreString;
import ir.webutils.*;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class PageRankSpider extends Spider {
    private Map<String, String> realToVirtualPageNameDictionary = new HashMap<>();
    private Map<String, String> virtualToRealPageNameDictionary = new HashMap<>();
    private Graph graph = new Graph();

    public static void main(String[] args) {
        new PageRankSpider().go(args);
    }

    @Override
    public void go(String[] args) {
        super.go(args);

        renameGraph();

        System.out.println(printGraphToString());

        Map<Node, Double> pageRanks = PageRankFile.calculatePageRank(graph, 0.15, 50);

        System.out.println(PageRankFile.printPageRanksToString(pageRanks));

        try {
            PageRankFile.savePageRanksToFile(pageRanks, saveDir);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void processPage(HTMLPage page) {
        super.processPage(page);
        String pageFilePath = page.getLink().getURL().getFile();
        String pageFileName = pageFilePath.substring(pageFilePath.lastIndexOf('/')+1, pageFilePath.length());
        String virtualPageName = "P" +  MoreString.padWithZeros(count,(int)Math.floor(MoreMath.log(maxCount, 10)) + 1)  + ".html";
        realToVirtualPageNameDictionary.put(pageFileName, virtualPageName);
        virtualToRealPageNameDictionary.put(virtualPageName, pageFileName);
        for (Object outLink : page.getOutLinks()) {
            String outLinkFilePath = ((Link) outLink).getURL().getFile();
            String outLinkFileName = outLinkFilePath.substring(outLinkFilePath.lastIndexOf('/')+1, outLinkFilePath.length());
            graph.addEdge(pageFileName, outLinkFileName);
        }
    }

    private void renameGraph() {
        Graph renamedGraph = new Graph();
        for(Node node : graph.nodeArray()) {
            String nodeName = realToVirtualPageNameDictionary.get(node.toString());
            for(Object nodeOutObject : node.getEdgesOut()) {
                Node nodeOut = (Node) nodeOutObject;
                String nodeOutName = realToVirtualPageNameDictionary.get(nodeOut.toString());
                renamedGraph.addEdge(nodeName, nodeOutName);
            }
        }
        graph = renamedGraph;
    }

    private String printGraphToString() {
        Node node;
        String answer = "";
        graph.resetIterator();
        while ((node = graph.nextNode()) != null) {
            answer += (node + "->" + node.getEdgesOut() + "\n");
        }
        return answer;
    }
}