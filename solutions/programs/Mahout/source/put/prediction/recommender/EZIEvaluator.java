package put.prediction.recommender;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.TanimotoCoefficientSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.common.RandomUtils;

import java.io.File;
import java.io.IOException;

/*
    Best recommender: Eucledian with Threshold 0.5, score: 0.953454986763274
    For Tanimoto Threshold we got NaN - not diagnosed...
 */

public class EZIEvaluator {
    private static DataModel model;

    private EZIEvaluator() {
    }

    public static void main(String args[]) throws IOException, TasteException {
        RandomUtils.useTestSeed();

        model = new FileDataModel(new File("data/movies.csv"));

        System.out.print("Eucledian - threshold 0.5: ");
        evaluate(getEucledianThresholdRecommenderBuilder(0.5));
        System.out.print("Eucledian - threshold 0.7: ");
        evaluate(getEucledianThresholdRecommenderBuilder(0.7));
        System.out.print("Eucledian - NN 5: ");
        evaluate(getEucledianNNRecommenderBuilder(5));
        System.out.print("Eucledian - NN 9: ");
        evaluate(getEucledianNNRecommenderBuilder(9));

        System.out.print("Pearson - threshold 0.5: ");
        evaluate(getPearsonThresholdRecommenderBuilder(0.5));
        System.out.print("Pearson - threshold 0.7: ");
        evaluate(getPearsonThresholdRecommenderBuilder(0.7));
        System.out.print("Pearson - NN 5: ");
        evaluate(getPearsonNNRecommenderBuilder(5));
        System.out.print("Pearson - NN 9: ");
        evaluate(getPearsonNNRecommenderBuilder(9));

        System.out.print("Tanimoto - threshold 0.5: ");
        evaluate(getTanimotoThresholdRecommenderBuilder(0.5));
        System.out.print("Tanimoto - threshold 0.7: ");
        evaluate(getTanimotoThresholdRecommenderBuilder(0.7));
        System.out.print("Tanimoto - NN 5: ");
        evaluate(getTanimotoNNRecommenderBuilder(5));
        System.out.print("Tanimoto - NN 9: ");
        evaluate(getTanimotoNNRecommenderBuilder(9));

    }

    public static void evaluate(RecommenderBuilder recommenderBuilder) throws TasteException {

        // evaluator - root mean square difference
        RecommenderEvaluator evaluator = new RMSRecommenderEvaluator();

        // 0.7 - 70% learning data, 1.0 - 100% test data
        double score = evaluator.evaluate(recommenderBuilder, null, model, 0.7, 1.0);

        System.out.println(score);
    }

    public static RecommenderBuilder getEucledianThresholdRecommenderBuilder(double threshold) {
        return dataModel -> {
            UserSimilarity similarity = new EuclideanDistanceSimilarity(dataModel);
            UserNeighborhood neighborhood = new ThresholdUserNeighborhood(threshold, similarity, dataModel);
            return new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        };
    }

    public static RecommenderBuilder getEucledianNNRecommenderBuilder(int nn) {
        return dataModel -> {
            UserSimilarity similarity = new EuclideanDistanceSimilarity(dataModel);
            UserNeighborhood neighborhood = new NearestNUserNeighborhood(nn, similarity, dataModel);
            return new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        };
    }


    public static RecommenderBuilder getPearsonThresholdRecommenderBuilder(double threshold) {
        return dataModel -> {
            UserSimilarity similarity = new PearsonCorrelationSimilarity(dataModel);
            UserNeighborhood neighborhood = new ThresholdUserNeighborhood(threshold, similarity, dataModel);
            return new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        };
    }

    public static RecommenderBuilder getPearsonNNRecommenderBuilder(int nn) {
        return dataModel -> {
            UserSimilarity similarity = new PearsonCorrelationSimilarity(dataModel);
            UserNeighborhood neighborhood = new NearestNUserNeighborhood(nn, similarity, dataModel);
            return new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        };
    }


    public static RecommenderBuilder getTanimotoThresholdRecommenderBuilder(double threshold) {
        return dataModel -> {
            UserSimilarity similarity = new TanimotoCoefficientSimilarity(dataModel);
            UserNeighborhood neighborhood = new ThresholdUserNeighborhood(threshold, similarity, dataModel);
            return new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        };
    }

    public static RecommenderBuilder getTanimotoNNRecommenderBuilder(int nn) {
        return dataModel -> {
            UserSimilarity similarity = new TanimotoCoefficientSimilarity(dataModel);
            UserNeighborhood neighborhood = new NearestNUserNeighborhood(nn, similarity, dataModel);
            return new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        };
    }

}
