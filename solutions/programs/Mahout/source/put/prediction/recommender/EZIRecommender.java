package put.prediction.recommender;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class EZIRecommender {

    private EZIRecommender() {
    }

    public static void main(String[] args) throws IOException, TasteException {
        //feed model with data read from file
        DataModel model = new FileDataModel(new File("data/movies.csv"));

        //define user similarity as Eucledian Distance
        UserSimilarity similarity = new EuclideanDistanceSimilarity(model);

        //neighborhood = threshold 0.7
        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.7, similarity, model);

        // recommender instance - User-based CF
        GenericUserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);

        for(LongPrimitiveIterator users = model.getUserIDs(); users.hasNext();){

            long userID = users.nextLong();

            //get 3 most recommended items
            List<RecommendedItem> recommendations = recommender.recommend(userID, 3);

            //write 3 recommended items
            for (RecommendedItem recommendation : recommendations){

                System.out.println(userID + "," + recommendation.getItemID() + "," + recommendation.getValue());
            }
        }

        /*
            Top 3 recommendation for user 943:
                RecommendedItem[item:258, value:5.0]
                RecommendedItem[item:300, value:4.3333335]
                RecommendedItem[item:751, value:4.0]
        */
    }
}
